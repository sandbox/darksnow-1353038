<?php

/**
 * @file
 *
 * Web UI for configuration of migration from PHPBB3 to Drupal.
 */

/**
 * Menu callback: Returns a page for configuring phpbb3 migrations.
 */
function phpbb3_migrate_configure() {
  drupal_set_title(t('Phpbb3 migrate configuration'));
  return drupal_get_form('phpbb3_migrate_configure_form');
}

/**
 * Form for configuring phpbb3 migrations.
 */
function phpbb3_migrate_configure_form($form, &$form_state) {

  $form['overview'] = array(
    '#prefix' => '<div>',
    '#markup' => t('The import of phpbb3 forums into Drupal can be configured using
      this form.<br />
      Once the configuration has been saved here, the various migrations will appear
      in the Migrate tab.<br />
      You may either provide the necessary credentials for Drupal to access
      your phpbb3 database and files directly, or the data must be in the current
      Drupal database and all files should have been copied into public storage.'),
    '#suffix' => '</div>',
  );

  // Select default text format for bodies etc.
  $options = array();
  foreach (filter_formats() as $format_id => $format) {
    $options[$format_id] = $format->name;
  }
  $form['phpbb3_migrate_text_format'] = array(
    '#type' => 'select',
    '#title' => t('Format for text fields'),
    '#default_value' => variable_get('phpbb3_migrate_text_format', 'filtered_html'),
    '#options' => $options,
    '#description' => t('Which input format to use for all content'),
  );

/*
  // Might need this bit for import of attachments etc
  if (module_exists('media') && !module_exists('migrate_extras')) {
    $form['need_extras'] = array(
      '#prefix' => '<div>',
      '#markup' => t('You have the <a href="@media">Media module</a> enabled - to
        take advantage of Media features, you need to also install and enable the
        <a href="@extras">Migrate Extras module</a>.',
        array('@media' => url('http://drupal.org/project/media'),
          '@extras' => url('http://drupal.org/project/migrate_extras'))),
      '#suffix' => '</div>',
    );
  }
 */
  $form['files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Phpbb3 files location'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['files']['description'] = array(
    '#prefix' => '<div>',
    '#markup' => t('To import your forum files (avatars, smilies, attachments,
                    etc) from phpbb3, enter the location here. You can point the
                    migration at the location of the website directly, or pick one
                    of the directories in the Drupal public file system. The
                    migration will look for the relevant directories, as determined
                    from the phpbb configuration, under this directory, only if you
                    have not set a domain.'),
    '#suffix' => '</div>',
  );

  $form['files']['phpbb3_migrate_files_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain of your forum'),
    '#default_value' => variable_get('phpbb3_migrate_files_domain'),
    '#description' => t('Enter the domain of the forum to import, with path if
                         necessary (e.g., http://example.phpbb3.com/forum).'),
  );

  $dirs = array('' => '<none>');
  $path = drupal_realpath('public://');
  foreach (scandir($path) as $dir) {
    $dir_path = drupal_realpath('public://' . $dir);
    if (drupal_substr($dir, 0, 1) != '.' && is_dir($dir_path)) {
      $dirs[$dir] = $dir;
    }
  }
  $form['files']['phpbb3_migrate_files_directory'] = array(
    '#type' => 'select',
    '#title' => t('Directory of PHPBB3 files'),
    '#default_value' => variable_get('phpbb3_migrate_files_directory', ''),
    '#options' => $dirs,
    '#description' => t('Enter the directory name of the forum files in local storage.'),
  );

  $form['db'] = array(
    '#type' => 'fieldset',
    '#title' => t('PHPBB3 database location'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  global $databases;

  $form['db']['phpbb3_migrate_db_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('PHPBB3 Table prefix'),
    '#default_value' => variable_get('phpbb3_migrate_db_prefix', 'phpbb_'),
    '#description' => t('The prefix on all table names in the phpbb database'),
  );

  $form['db']['phpbb3_migrate_db_local'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use local database'),
    '#default_value' => variable_get('phpbb3_migrate_db_local', 1),
    '#description' => t('The PHPBB3 data has been imported into the Drupal database, ignore credentials below'),
  );

  $form['db']['phpbb3_migrate_db_driver'] = array(
    '#type' => 'select',
    '#title' => t('DB Driver'),
    '#default_value' => db_driver(),
    '#options' => array('mysql' => 'MySQL', 'pgsql' => 'PostgreSQL', 'sqlite' => 'SQL Lite'),
    '#description' => t('The type of database server to connect to'),
  );

  $form['db']['phpbb3_migrate_db_host'] = array(
    '#type' => 'textfield',
    '#title' => t('DB Hostname'),
    '#default_value' => variable_get('phpbb3_migrate_db_host', $databases['default']['default']['host']),
    '#description' => t('The hostname of the PHPBB3 database server'),
  );

  $form['db']['phpbb3_migrate_db_dbname'] = array(
    '#type' => 'textfield',
    '#title' => t('Database name'),
    '#default_value' => variable_get('phpbb3_migrate_db_dbname', $databases['default']['default']['database']),
    '#description' => t('Name of the database on host server'),
  );

  $form['db']['phpbb3_migrate_db_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('phpbb3_migrate_db_username', $databases['default']['default']['username']),
    '#description' => t('Username used to access database'),
  );

  $form['db']['phpbb3_migrate_db_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Password to your phpbb3 database'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Validate callback for the phpbb3 configure form.
 */
function phpbb3_migrate_configure_form_validate($form, &$form_state) {

  if ($form_state['values']['phpbb3_migrate_files_domain'] == '' &&
      $form_state['values']['phpbb3_migrate_files_directory'] == '') {
    form_set_error('', t('You must set a domain or directory to find forum files.'));
  }

  if ($form_state['values']['phpbb3_migrate_db_local'] == 0 &&
      $form_state['values']['phpbb3_migrate_db_host'] == '' &&
      $form_state['values']['phpbb3_migrate_db_dbname'] == '' &&
      $form_state['values']['phpbb3_migrate_db_username'] == '' &&
      $form_state['values']['phpbb3_migrate_db_password'] == '') {
    form_set_error('', t('All external database credentials must be set if
                          you are not using the local database.'));
  }

  $domain = $form_state['values']['phpbb3_migrate_files_domain'];
  if (drupal_substr($domain, -1) != '/') {
      $domain .= '/';
  }
  if (drupal_strlen($domain) > 0 && drupal_substr($domain, 0, 7) != 'http://') {
    $domain = "http://{$domain}";
  }
  $form_state['values']['phpbb3_migrate_files_domain'] = $domain;
}

/**
 * Submit callback for the phpbb3 configure form.
 */
function phpbb3_migrate_configure_form_submit($form, &$form_state) {
  variable_set('phpbb3_migrate_text_format', $form_state['values']['phpbb3_migrate_text_format']);
  variable_set('phpbb3_migrate_db_prefix', $form_state['values']['phpbb3_migrate_db_prefix']);
  variable_set('phpbb3_migrate_db_local', $form_state['values']['phpbb3_migrate_db_local']);
  variable_set('phpbb3_migrate_db_driver', $form_state['values']['phpbb3_migrate_db_driver']);
  variable_set('phpbb3_migrate_db_host', $form_state['values']['phpbb3_migrate_db_host']);
  variable_set('phpbb3_migrate_db_dbname', $form_state['values']['phpbb3_migrate_db_dbname']);
  variable_set('phpbb3_migrate_db_username', $form_state['values']['phpbb3_migrate_db_username']);
  variable_set('phpbb3_migrate_files_domain', $form_state['values']['phpbb3_migrate_files_domain']);
  variable_set('phpbb3_migrate_files_directory', $form_state['values']['phpbb3_migrate_files_directory']);
  if ($form_state['values']['phpbb3_migrate_db_password']) {
    variable_set('phpbb3_migrate_db_password', $form_state['values']['phpbb3_migrate_db_password']);
  }
  PhpbbConfig::registerMigrations();
  drupal_set_message(t('PHPBB3 configuration changes saved.'));
}
