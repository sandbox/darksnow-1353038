<?php

/**
 * @file
 * Drush support for phpbb3 migration module
 */

/**
 * Implements hook_drush_help().
 */
function migrate_phpbb3_drush_help($section) {
  switch ($section) {
    case 'drush:phpbb3-rename-attachments':
      return dt('Rename all phpbb attachments from random name to real name');
    case 'drush:phpbb3-rename-rollback':
      return dt('Rollback a WordPress blog');
  }
}

/**
 * Implements of hook_drush_command().
 */
function migrate_phpbb3_drush_command() {
  static $commands = FALSE;

  $items['phpbb3-rename-attachments'] = array(
    'description' => 'Rename all phpbb attachments from random name to real name',
    'arguments' => array(),
    'examples' => array(
      'phpbb3-rename-attachments' => 'Rename Files',
    ),
    'drupal dependencies' => array('migrate_phpbb3'),
  );
  $items['phpbb3-rename-rollback'] = array(
    'description' => 'Rename all phpbb attachments back to random name',
    'arguments' => array(),
    'examples' => array(
      'phpbb3-rename-rollback' => 'Rename Files',
    ),
    'drupal dependencies' => array('migrate_phpbb3'),
  );
  return $items;
}

/**
 * Rename all the files in the attachements table.
 *
 * @param string $from Database field containing old file name
 * @param string $to   Database field containing new file name
 * @param bool $change Should we change to the files_directory is configured
 */
function drush_migrate_phpbb3_do_rename($from, $to, $path = FALSE) {
  if (!PhpbbConfig::getVar('text_format')) {
    drush_log('Please ensure you have configured the phpbb3 migration', 'error');
    return;
  }

  $dir = $path ? $path : PhpbbConfig::getVar('files_directory');
  if (drupal_substr($dir, -1) != '/') {
    $dir .= '/';
  }

  PhpbbConfig::createConnection();
  $result = Database::getConnection('default', 'phpbb')
    ->select('{attachments}', 'a')
    ->fields('a', array('real_filename', 'physical_filename'))
    ->execute();

  $count = 0;
  while ($row = $result->fetchAssoc()) {
    if (@rename("file://{$dir}{$row[$from]}", "file://{$dir}{$row[$to]}")) {
      $count++;
    }
    else {
      drush_log("Unable to rename {$row[$from]}", 'warning');
    }
  }

  if ($count == 0) {
    drush_log('No files renamed. If you have a domain set, then this command must be run with the path to the directory that contains the files to be renamed.', 'error');
  }
  else {
    drush_log("{$count} files renamed.");
  }
}

function drush_migrate_phpbb3_rename_attachments($path = '') {
  drush_migrate_phpbb3_do_rename('physical_filename', 'real_filename', $path);
}

function drush_migrate_phpbb3_rename_rollback($path = '') {
  drush_migrate_phpbb3_do_rename('real_filename', 'physical_filename', $path);
}